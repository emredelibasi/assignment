//
//  Article.swift
//  Assignment
//
//  Created by Emre Delibasi on 5/10/17.
//  Copyright © 2017 ALeapFrog. All rights reserved.
//

import UIKit

class Article: NSObject {
    
    let name: String
    let image: UIImage
    
    init(name: String, image: UIImage) {
        self.name = name
        self.image = image
        super.init()
    }
}
