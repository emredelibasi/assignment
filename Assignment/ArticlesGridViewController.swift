//
//  ViewController.swift
//  Assignment
//
//  Created by Emre Delibasi on 5/10/17.
//  Copyright © 2017 ALeapFrog. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
import SnapKit

class ArticlesGridViewController: UIViewController {
    
    fileprivate var searchController: UISearchController!
    fileprivate var collectionView: UICollectionView!
    
    var articles = [Article]()
    var filteredArticles = [Article]()
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureNavigationController()
        configureCollectionView()
        configureSearchController()
        initializeArticles()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unregisterKeyboardNotifications()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        collectionView.snp.makeConstraints { make in
            make.top.equalTo(self.view.snp.top).offset(44)
            make.right.equalTo(self.view.snp.right)
            make.left.equalTo(self.view.snp.left)
            make.bottom.equalTo(self.view.snp.bottom)
        }
    }
    
    // MARK: - Initialization/Configuration
    
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardDidShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    func configureNavigationController() {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.title = "Assignment"
    }
    
    func configureSearchController() {
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search image"
        definesPresentationContext = true
        searchController.hidesNavigationBarDuringPresentation = true
        searchController.searchBar.sizeToFit()

        self.view.addSubview(searchController.searchBar)
    }
    
    func configureCollectionView() {
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSize(width: itemBorderForScreen(), height: itemBorderForScreen())
        flowLayout.minimumInteritemSpacing = Constants.gridViewMargin
        flowLayout.minimumLineSpacing = Constants.gridViewMargin
        flowLayout.sectionInset = UIEdgeInsets(top: Constants.gridViewMargin, left: Constants.gridViewMargin, bottom: Constants.gridViewMargin, right: Constants.gridViewMargin)
        flowLayout.scrollDirection = .vertical
        collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: flowLayout)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(ArticleCell.self, forCellWithReuseIdentifier: "ArticleCell")
        collectionView.register(UINib(nibName: "ArticleCell", bundle: nil), forCellWithReuseIdentifier: "ArticleCell")
        
        customizeCollectionView()
        
        self.view.addSubview(collectionView)
    }

    func customizeCollectionView() {
        collectionView.backgroundColor = .white
    }
    
    func initializeArticles() {
        let articleCount = Constants.articleNames.count
        
        let dispatchGroup = DispatchGroup()
        
        for i in 0...articleCount - 1 {
            dispatchGroup.enter()
            guard let verifiedUrl = URL(string: Constants.articleURLs[i]) else {
                continue
            }
            
            Alamofire.request(verifiedUrl).responseImage(completionHandler: { response in
                if let image = response.result.value {
                    let article = Article(name: Constants.articleNames[i], image: image)
                    self.articles.append(article)
                }
                dispatchGroup.leave()
            })
        }
        
        dispatchGroup.notify(queue: .main) {
            self.collectionView.reloadData()
        }
    }
    
    // MARK: - Helpers
    
    func indexForArticleInList(article: Article) -> Int {
        return articles.index(of: article)!.hashValue
    }
    
    func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardDidShow(notification: NSNotification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        let contentInsets = UIEdgeInsets(top: Constants.gridViewMargin, left: 0, bottom: keyboardSize.height, right: 0)
        collectionView.contentInset = contentInsets
        collectionView.scrollIndicatorInsets = contentInsets
    }
    
    func keyboardWillHide(notification: NSNotification) {
        collectionView.contentInset = UIEdgeInsets(top: Constants.gridViewMargin, left: 0, bottom: 0, right: 0)
        collectionView.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        
        filteredArticles = articles.filter { article in
            return article.name.lowercased().contains(searchText.lowercased())
        }
        
        collectionView.reloadData()
    }
    
    func itemBorderForScreen() -> CGFloat {
        return (self.view.frame.width - 3 * Constants.gridViewMargin) / 2
    }
    
    // MARK: - Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showPagerSegue" {
            if let aPC = segue.destination as? ArticlesPagerController, let indexPath = sender as? IndexPath {
                
                let userSearching = searchController.isActive && searchController.searchBar.text != ""
                let article = userSearching ? filteredArticles[indexPath.row] : articles[indexPath.row]
                
                aPC.scrollIndex = indexForArticleInList(article: article)
                aPC.articles = articles
                aPC.navigationItem.title = article.name
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension ArticlesGridViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showPagerSegue", sender: indexPath)
    }
    
    
}

extension ArticlesGridViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredArticles.count
        }
        return articles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ArticleCell", for: indexPath) as! ArticleCell
        
        let article: Article
        
        if searchController.isActive && searchController.searchBar.text != "" {
            article = filteredArticles[indexPath.row]
        } else {
            article = articles[indexPath.row]
        }
        
        cell.article = article
        cell.imageView.image = article.image
        
        return cell
    }
}

extension ArticlesGridViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchText: searchController.searchBar.text!)
    }
}


