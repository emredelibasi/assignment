//
//  ArticlesPagerController.swift
//  Assignment
//
//  Created by Emre Delibasi on 5/13/17.
//  Copyright © 2017 ALeapFrog. All rights reserved.
//

import UIKit

class ArticlesPagerController: UIViewController {

    fileprivate var collectionView: UICollectionView!
    var articles = [Article]()
    var scrollIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureCollectionView()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        collectionView.snp.makeConstraints { make in
            make.top.equalTo(self.view.snp.top)
            make.right.equalTo(self.view.snp.right)
            make.left.equalTo(self.view.snp.left)
            make.bottom.equalTo(self.view.snp.bottom)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        DispatchQueue.main.async {
            self.collectionView.scrollToItem(at: IndexPath(item: self.scrollIndex, section: 0), at: .init(rawValue: 0), animated: false)
        }
    }
    
    // MARK: - Initialization
    
    func configureCollectionView() {
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSize(width: self.view.frame.width, height: self.view.frame.height)
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.minimumLineSpacing = 0
        flowLayout.sectionInset = UIEdgeInsets.zero
        flowLayout.scrollDirection = .horizontal
        collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: flowLayout)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isPagingEnabled = true
        collectionView.register(ArticleCell.self, forCellWithReuseIdentifier: "ArticleCell")
        collectionView.register(UINib(nibName: "ArticleCell", bundle: nil), forCellWithReuseIdentifier: "ArticleCell")
        
        self.view.addSubview(collectionView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        scrollIndex = 0
    }
}

extension ArticlesPagerController: UICollectionViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let indexPath = collectionView.indexPathsForVisibleItems[0]
        
        guard let articleCell = collectionView.cellForItem(at: indexPath) as? ArticleCell else {
            return
        }
        
        DispatchQueue.main.async {
            self.navigationItem.title = articleCell.article.name
        }
    }
}

extension ArticlesPagerController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return articles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ArticleCell", for: indexPath) as! ArticleCell
        
        let article = articles[indexPath.row]
                
        cell.article = article
        cell.imageView.image = article.image
        
        return cell
    }
}
