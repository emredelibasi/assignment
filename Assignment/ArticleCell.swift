//
//  ArticleCell.swift
//  Assignment
//
//  Created by Emre Delibasi on 5/13/17.
//  Copyright © 2017 ALeapFrog. All rights reserved.
//

import UIKit

class ArticleCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    var article: Article!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imageView.contentMode = .scaleAspectFill
    }

}
