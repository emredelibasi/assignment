//
//  Constants.swift
//  Assignment
//
//  Created by Emre Delibasi on 5/11/17.
//  Copyright © 2017 ALeapFrog. All rights reserved.
//

import UIKit

class Constants: NSObject {
    
    static let articleNames = ["Bicycle", "Strawberries", "Amsterdam", "Lion", "Dolphin", "Ship", "Forest", "Northern Lights"]
    static let articleURLs =  ["https://drscdn.500px.org/photo/388736/m%3D900/228522b74237e2b6174e7a7c397b58f4", "https://drscdn.500px.org/photo/40815510/m%3D900/81e0d26501a20d4211bb68635b26ce7f", "https://drscdn.500px.org/photo/211390263/m%3D900/907c29dc01d61302176a38ac845f2be6", "https://drscdn.500px.org/photo/211670507/m%3D900/588b295c64583c74712fde1382b74c53", "https://drscdn.500px.org/photo/211317681/m%3D900/76572f4d3c4cc8c908ab54c6b181b767", "https://drscdn.500px.org/photo/211311111/m%3D900/e7ed221a2e69f16e22f99d7996a2ecfb", "https://drscdn.500px.org/photo/211393695/m%3D900/f6dd041cdb14c111e14c8556bb84918b", "https://drscdn.500px.org/photo/211393421/m%3D900/20e38becb5dacc23b9fb847e375e31cd"]
    static let gridViewMargin: CGFloat = 20
    static var statusBarHeight: CGFloat {
        get {
            return UIApplication.shared.statusBarFrame.height
        }
    }
}
